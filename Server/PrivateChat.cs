﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace Server
{
    public partial class PrivateChat : Form
    {
        private SeverForm serverForm;

        public PrivateChat(SeverForm serverFormInstance)
        {
            InitializeComponent();
            serverForm = serverFormInstance;
        }

        private void buttonSend_Click(object sender, EventArgs e)
        {
            if (txtInput.Text != string.Empty)
            {
                foreach (var client in from ListViewItem item in serverForm.clientList.SelectedItems select (Client) item.Tag)
                {
                    client.Send("pMessage|" + txtInput.Text);                   
                }
                txtReceive.Text += "Server says: " + txtInput.Text + "\r\n";
                txtInput.Text = string.Empty;
            }
        }

        private void textBoxInput_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnSend.PerformClick();
            }
        }

        private void textBoxReceive_TextChanged(object sender, EventArgs e)
        {
            txtReceive.SelectionStart = txtReceive.TextLength;
        }
    }
}