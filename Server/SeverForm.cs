﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Windows.Forms;

namespace Server
{
    public partial class SeverForm : Form
    {
        public const int SERVER_PORT = 11000;

        private PrivateChat privateChat;

        private Listener listener;

        public List<Socket> clients = new List<Socket>(); // store all the clients into a list

        public void BroadcastData(string data) // send to all clients
        {
            for (int i = 0; i < clients.Count; i++)
            {
                try { clients[i].Send(Encoding.UTF8.GetBytes(data)); }
                catch (Exception) { Console.Beep(1000, 1000); } 
            }
        }

        public SeverForm()
        {
            privateChat = new PrivateChat(this);
            InitializeComponent();
            listener = new Listener(SERVER_PORT);
            listener.SocketAccepted += listener_SocketAccepted;
        }

        private void listener_SocketAccepted(Socket _socket)
        {
            Client client = new Client(_socket);
            client.Received += client_Received;
            client.Disconnected += client_Disconnected;
            this.Invoke(() =>
            {
                string ip = client.Ip.ToString().Split(':')[0];
                var item = new ListViewItem(ip); // ip
                item.SubItems.Add(" "); // nickname
                item.SubItems.Add(" "); // status
                item.Tag = client;
                clientList.Items.Add(item);
                clients.Add(_socket);
            });
        }

        private void client_Disconnected(Client sender)
        {
            this.Invoke(() =>
            {
                for (int i = 0; i < clientList.Items.Count; i++)
                {
                    var client = clientList.Items[i].Tag as Client;
                    if (client.Ip == sender.Ip)
                    {
                        textBoxReceive.Text += "<< " + clientList.Items[i].SubItems[1].Text + " has left the room >>\r\n";
                        BroadcastData("RefreshChat|" + textBoxReceive.Text);
                        clientList.Items.RemoveAt(i);
                    }
                }
            });
        }


        private void client_Received(Client sender, byte[] data)
        {
            this.Invoke(() =>
            {
                for (int i = 0; i < clientList.Items.Count; i++)
                {
                    var client = clientList.Items[i].Tag as Client;

                    if (client == null || client.Ip != sender.Ip) continue;

                    var command = Encoding.UTF8.GetString(data).Split('|');

                    switch (command[0])
                    {
                        case "Connect":
                            textBoxReceive.Text += "<< " + command[1] + " joined the room >>\r\n";

                            clientList.Items[i].SubItems[1].Text = command[1]; // nickname
                            clientList.Items[i].SubItems[2].Text = command[2]; // status

                            string users = string.Empty;

                            for (int j = 0; j < clientList.Items.Count; j++)
                            {
                                users += clientList.Items[j].SubItems[1].Text + "|";
                            }

                            BroadcastData("Users|" + users.TrimEnd('|'));
                            BroadcastData("RefreshChat|" + textBoxReceive.Text);

                            break;
                        case "Message":
                            textBoxReceive.Text += command[1] + " says: " + command[2] + "\r\n";
                            BroadcastData("RefreshChat|" + textBoxReceive.Text);
                            break;
                        case "pMessage":
                            this.Invoke(() =>
                            {
                                privateChat.txtReceive.Text += command[1] + " says: " + command[2] + "\r\n";
                            });
                            break;
                        case "pChat":

                            break;
                    }
                }
            });
        }

        private void Main_Load(object sender, EventArgs e)
        {
            listener.Start();
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);
            listener.Stop();
        }

        private void sendButton_Click(object sender, EventArgs e)
        {
            if (textBoxInput.Text != string.Empty)
            {
                BroadcastData("Message|" + "Server: " + textBoxInput.Text);
                textBoxReceive.Text += textBoxInput.Text + "\r\n";
                textBoxInput.Text = "";
            }
        }

        private void disconnectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (var client in from ListViewItem item in clientList.SelectedItems select (Client) item.Tag)
            {
                client.Send("Disconnect|");
            }
        }

        private void chatWithClientToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (var client in from ListViewItem item in clientList.SelectedItems select (Client) item.Tag)
            {
                client.Send("Chat|");
                privateChat = new PrivateChat(this);
                privateChat.Show();
            }
        }

        private void textBoxInput_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                sendButton.PerformClick();
        }

        private void textBoxReceive_TextChanged(object sender, EventArgs e)
        {
            textBoxReceive.SelectionStart = textBoxReceive.TextLength;
        }
    }
}