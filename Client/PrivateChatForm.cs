﻿using System;
using System.Windows.Forms;

namespace Client
{
    public partial class PrivateChatForm : Form
    {
        public PublicChatForm publicChatForm;

        public PrivateChatForm(PublicChatForm publicChatInstance)
        {
            InitializeComponent();
            publicChatForm = publicChatInstance;
        }

        private void buttonSend_Click(object sender, EventArgs e)
        {
            if (textBoxInput.Text != string.Empty)
            {
                string user = Text.Split('-')[1];

                publicChatForm.formLogin.Client.Send("pMessage|" + user + "|" + textBoxInput.Text);
                textBoxReceive.Text += user + " says: " + textBoxInput.Text + "\r\n";
                textBoxInput.Text = string.Empty;
            }
        }

        private void textBoxInput_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            { buttonSend.PerformClick(); }
        }

        private void textBoxReceive_TextChanged(object sender, EventArgs e)
        {
            textBoxReceive.SelectionStart = textBoxReceive.TextLength;
        }
    }
}