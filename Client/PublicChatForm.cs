﻿using System;
using System.Windows.Forms;

namespace Client
{
    public partial class PublicChatForm : Form
    {
        public const int DEFAULT_SERVER_PORT = 11000;

        private PrivateChatForm privateChatForm;

        public LoginForm formLogin = new LoginForm();

        public PublicChatForm()
        {
            privateChatForm = new PrivateChatForm(this);
            InitializeComponent();
        }


         //  Text = "TCP Chat - " + formLogin.txtIP.Text + " - (Connected as: " + formLogin.textBoxNickname.Text + ")";
     
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            formLogin.Client.Received += client_Received;
            formLogin.Client.Disconnected += Client_Disconnected;
            formLogin.ShowDialog();
        }

        private static void Client_Disconnected(ClientSettings cs)
        {
                              
        }


        public void client_Received(ClientSettings cs, string received)
        {
            string[] command = received.Split('|');
            switch (command[0])
            {
                case "Users":
                    this.Invoke(() =>
                    {
                        userList.Items.Clear();
                        for (int i = 1; i < command.Length; i++)
                        {
                            if (command[i] != "Connected" | command[i] != "RefreshChat")
                            {
                                userList.Items.Add(command[i]);
                            }
                        }
                    });
                    break;
                case "Message":
                    this.Invoke(() =>
                    {
                        textBoxReceive.Text += command[1] + "\r\n";
                    });
                    break;
                case "RefreshChat":
                    this.Invoke(() =>
                    {
                        textBoxReceive.Text = command[1];
                    });
                    break;
                case "Chat":
                    this.Invoke(() =>
                    {
                        privateChatForm.Text = privateChatForm.Text.Replace("user", formLogin.textBoxNickname.Text);
                        privateChatForm.Show();
                    });
                    break;
                case "pMessage":
                    this.Invoke(() =>
                    {
                        privateChatForm.textBoxReceive.Text += "Server says: " + command[1] + "\r\n";
                    });
                    break;
                case "Disconnect":
                    Application.Exit();
                    break;
            }
        }

        private void buttonSend_Click(object sender, EventArgs e)
        {
            if (textBoxInput.Text != string.Empty)
            {
                formLogin.Client.Send("Message|" + formLogin.textBoxNickname.Text + "|" + textBoxInput.Text);
                textBoxReceive.Text += formLogin.textBoxNickname.Text + " says: " + textBoxInput.Text + "\r\n";
                textBoxInput.Text = string.Empty;
            }
        }

        private void txtInput_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                buttonSend.PerformClick();
            }
        }

        private void txtReceive_TextChanged(object sender, EventArgs e)
        {
            textBoxReceive.SelectionStart = textBoxReceive.TextLength;
        }

        private void privateChat_Click(object sender, EventArgs e)
        {
            formLogin.Client.Send("pChat|" + formLogin.textBoxNickname.Text);
        }

      
    }
}