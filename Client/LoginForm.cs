﻿using System;
using System.Windows.Forms;

namespace Client
{
    public partial class LoginForm : Form
    {
        public ClientSettings Client;

        public LoginForm(PublicChatForm publicChatFormInstance)
        {
        }

        public LoginForm()
        {
            // TODO: Complete member initialization
            InitializeComponent();
            Client = new ClientSettings();
        }

        private void buttonConnect_Click(object sender, EventArgs e)
        {
            if (textBoxNickname.Text != "")
            {
             
                Client.Connected += Client_Connected;
                Client.Connect(textBoxIP.Text, PublicChatForm.DEFAULT_SERVER_PORT);
                Client.Send("Connect|" + textBoxNickname.Text + "|connected");
            }
        }

        private void Client_Connected(object sender, EventArgs e)
        {
            this.Invoke(Close);
        }

     
    }
}