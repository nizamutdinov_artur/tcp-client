﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace Client
{
    public class ClientSettings
    {
        private Socket socket;
        public delegate void ReceivedEventHandler(ClientSettings cs, string received);
        public event ReceivedEventHandler Received = delegate { };
        public event EventHandler Connected = delegate { };
        public delegate void DisconnectedEventHandler(ClientSettings cs);
        public event DisconnectedEventHandler Disconnected = delegate {};

        /// <summary>
        /// Not used
        /// TODO: Use that variable to prevent unconnected client
        /// </summary>
        private bool connected; 

        public ClientSettings()
        {
            socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        }

        public void Connect(string ip, int port)
        {
            try
            {
                var ep = new IPEndPoint(IPAddress.Parse(ip), port);
                socket.BeginConnect(ep, ConnectCallback, socket);
            }
            catch { }
        }

        public void Close()
        {
            socket.Dispose();
            socket.Close();
        }

        void ConnectCallback(IAsyncResult ar)
        {
            try
            {
                socket.EndConnect(ar);
                connected = true;
                Connected(this, EventArgs.Empty);
                var buffer = new byte[socket.ReceiveBufferSize];

                socket.BeginReceive(buffer, 0, buffer.Length, SocketFlags.None, ReadCallback, buffer);
            }
                 catch (Exception exeption)
            {
                System.Windows.Forms.MessageBox.Show(exeption.ToString());
            }
        }

        private void ReadCallback(IAsyncResult ar)
        {
            var buffer = (byte[]) ar.AsyncState;
            int records = socket.EndReceive(ar);

            if (records != 0)
            {
                var data = Encoding.UTF8.GetString(buffer, 0, records);
                Received(this, data);
            }

            else
            {
                Disconnected(this);
                connected = false;
                Close();
                return;
            }
            socket.BeginReceive(buffer, 0, buffer.Length, SocketFlags.None, ReadCallback, buffer);
        }

        public void Send(string data)
        {
            try
            {
                var byteBuffer = Encoding.UTF8.GetBytes(data);
                socket.BeginSend(byteBuffer, 0, byteBuffer.Length, SocketFlags.None, SendCallback, byteBuffer);
            }
            catch 
            { Disconnected(this); }
        }

        void SendCallback(IAsyncResult ar)
        {
            socket.EndSend(ar);
        }
    }
}